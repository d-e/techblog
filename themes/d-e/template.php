<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8" />

  <title><?=$post['Title']?><?=isset($post['Title'])?" | ":""?><?=$site_title?></title>
  <meta name="description" content="<?=$site_description?>">
<?php if (isset($post['robots'])) { ?>
  <meta name="robots" content="<?=$post['robots']?>">
<?php } ?>
  <link rel="stylesheet" href="<?=$theme?>/style.css" type="text/css" />
  <link rel="icon" type="image/png" href="<?=$theme?>/images/favicon.png" />
  <link rel="apple-touch-icon" href="<?=$theme?>/images/touchicon.png"/>
  <link rel="shortcut icon" href="<?=$theme?>/images/touchicon.png" />
  <meta name="viewport" content="width=device-width">
  <!-- Piwik -->
  <script type="text/javascript">
    var _paq = _paq || [];
    _paq.push(["setDomains", ["*.social.d-e.gr/techblog"]]);
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="//social.d-e.gr/stats/piwik/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '4']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <noscript><p><img src="//social.d-e.gr/stats/piwik/piwik.php?idsite=4" style="border:0;" alt="" /></p></noscript>
  <!-- End Piwik Code -->

</head>
<body>

  <header id="header">
      <img id=logo style=white-space:pre src=<?=$theme?>/images/logo-pixelated-interlaced-fine.png alt="
////////////////:///////////////:-.
///////////-..`  -/////////////////:`
///////////`      ://///////////////:
//////////::      `::::....-:////////
///////:.``:-      ` `/.     .://////
//////-    `/.        -:`   .-://////
//////      ./`        ::://// //////
//////`      -:        `://// -//////
://///:.      :- ``     .:   :///////
`://////:-....://///:----:///////////
  .-:////////////////////////////////      " />
      <h1><a href="<?=$base_url?>"><?=$site_title?></a></h1>
      <ul class="nav">
        <?php foreach ($pages as $title=>$page) { ?>
        <li><a href="<?=$base_url.$page ?>"><?=$title ?></a></li>
        <?php } ?>
      </ul>
  </header>

    <?php if ($is_front_page) {?> <!-- Front page lists all blog posts -->

  <div id="posts">
        <?php foreach($posts as $post ){ ?>
    <div class="post">
      <h3><a href="<?=$base_url.$post['file'] ?>"><?=$post['meta']['Title'] ?></a></h3>
      <h4><?=$post['meta']['Author'] ?></h4>
      <p class="meta"><?=$post['meta']['formatted_date'] ?></p>
      <p class="excerpt"><?=$post['excerpt']?></p>
    </div>
        <?php } ?>
  </div>

    <?php } else { ?> <!-- Single page shows individual blog post -->

  <div class="post">
    <h2><?= $post['Title']?> </h2>
    <h3>by <?= $post['Author']?> </h3>
    <p class="meta"><?= $post['formatted_date'] ?></p>
    <?= $post_html ?>
  </div>

    <?php } ?>


  <footer id="footer">
    a <a href="http://d-e.gr">d-e.gr</a> website
  </footer>

</body>
</html>
