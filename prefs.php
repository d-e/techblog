<?php

$site_title = "d-e_techblog";
$site_description = "experience from IT and software development @d-e.gr";
$theme = "d-e";
$date_format = "d.m.y";
$excerpt_length = 600;  //The first 600 chars of every post will be visible in the homepage
date_default_timezone_set('Europe/Athens');
?>
