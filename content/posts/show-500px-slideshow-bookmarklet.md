```
Title: Start a 500px slideshow from your bookmarks bar
Description:
Author: Michael Demetriou
Date: 2016-12-21
```
--

Unfortunately there is no simple app that just slideshows nice images from 500px.

Drag this bookmarklet [500px slideshow](javascript:(function(){minutes=5;setInterval(function(){$('.photo-focus__nav--right').click()},minutes*60000);})();)
to your bookmarks toolbar to start one.

You'll have to be in a page that looks like this,
![500px fullscreen](../images/500px-fullscreen.jpg)
for it to work, so go to your favorite gallery (e.g. [popular](http://500px.com/popular)) click on the first image,
then again on it's center (where the cursor becomes a magnifying glass)
![500px zoom area](../images/500px-zoom-area.jpg)

and then click the bookmarklet. If you'd like to change the duration of each slide, just edit the
bookmarklet and change `minutes=5` to your desired value

Finally press <kbd>F11</kbd> to view the slideshow in fullscreen.

## PS

If you have a large screen, some images will be centered instead of fullscreen.
This depends on the size the photographer initially uploaded. This modified
bookmarklet stretches the pictures to full height. (Due to limitations of CSS
  this can't work both for width and height reliably yet)

[500px slideshow](javascript:(function(){minutes=5;setInterval(function(){$('.photo-focus__nav--right').click()},minutes*60000);$('.photo-focus__photo').css({'height':'100%','width':'100%','object-fit':'contain'});})();)


## PS2

I was too quick to speak. In the time that I've been inactive from the CSS 
world there has been a new property `object-fit` which does exactly what we need.
I have revised the above bookmarklet.
