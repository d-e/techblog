```
Title: How to successfully run nextcloud in a subdirectory
Description:
Author: Michael Demetriou
Date: 2019-05-23
```
--

I run a local nextcloud instance for "cloud" storage. I have installed it in https://example.com/nextcloud but nextcloud complains that 

> Your web server is not properly set up to resolve “/ocm-provider/”
> Your web server is not properly set up to resolve “/ocs-provider/”
> Your web server is not properly set up to resolve “/.well-known/caldav”.
> Your web server is not properly set up to resolve “/.well-known/carddav”.

Adding this `.htaccess` file to the root of my webserver (or it's contents in the apache configuration) solves all these issues.

        <IfModule mod_rewrite.c>
            RewriteEngine On
            RewriteRule ^\.well-known/host-meta nextcloud/public.php?service=host-meta [Q$
            RewriteRule ^\.well-known/host-meta\.json nextcloud/public.php?service=host-m$
            RewriteRule ^\.well-known/webfinger nextcloud/public.php?service=webfinger [Q$
            RewriteRule ^\.well-known/carddav /nextcloud/remote.php/dav/ [R=301,L]
            RewriteRule ^\.well-known/caldav /nextcloud/remote.php/dav/ [R=301,L]
            RewriteRule ^ocm-provider /nextcloud/ocm-provider/ [R=301,L]
            RewriteRule ^ocs-provider /nextcloud/ocs-provider/ [R=301,L]
        </IfModule>

And now, 

![All Checks Passed](../images/nextcloud-allchecks.png)

Well not really as I just upgraded to Nextcloud 16 and it now wants me to enable MySQL 4-byte char support because emoji in filenames (smh) 😠 📑

![Nextcloud Mysql 4-byte Warning](../images/settings-nextcloud-mysql-4byte.png)

