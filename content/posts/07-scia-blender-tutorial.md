```
Title: Create beautiful visualisations of your structural models with blender and sketchfab &mdash; part 1: renders.
Description:
Author: Michael Demetriou
Date: 2017-06-15
```
--

## Exporting your model

First off, you have to export the mesh of your model in 3D from the structural software of your choice. We use [SCIA engineer](https://www.scia.net/en/software/product-selection/scia-engineer) here at the office, so these are the instructions for that.

1. Open your model
2. Choose **Structural Model** from **View** > **Set view parameters**. This is required in order for SCIA to export solids and not linear elements.
![](../content/images/scia-blender-tutorial/scia-structural-model.png)
3. Choose **Export** > **Graphic Format** _![](../content/images/scia-blender-tutorial/scia-export-graphics.png)_
4. In the window that appears choose **wrl** as file type. _![](../content/images/scia-blender-tutorial/scia-export-vrml.png)_

##Converting the model in a format that Blender understands.
While Blender does have a `*.wrl` importer, it unfortunately hangs when presented with a model exported from SCIA Engineer. Not to worry however, as another Free Software tool will help us to successfully get our model into Blender.

Go download [Meshlab](http://www.meshlab.net/) or just `apt install meshlab` if you are on a debian based system. Import your `.wrl` into meshlab and **File** > **Export Mesh** as `.stl`

## Getting your model into Blender
If you are unfamilliar with [Blender](http://blender.org) it is a Free Software 3D modelling/rendering/animation suite with quite some capabilities.

Install Blender on your computer and fire it up. Remember that you have to right click to select things in blender. If you don't like this behaviour you can change it in **File** > **User Preferences...**

By now you should be seeing the default blender cube. Right click on it and press <kbd>Del</kbd> to delete it. Make sure to keep your mouse pointer still at that point (as moving it will dismiss the confirmation dialog box that appears) and click on **Delete** _![](../content/images/scia-blender-tutorial/Delete.jpg)_ (or press <kbd>Enter</kbd>).

Using **File** > **Import** > **Stl**, import your `.stl` file. It should appear inside your 3D View. It is possible that your model appears side up, or upside down. There are two ways to fix that. Make sure it is selected and do one of the following.

* Press <kbd>R</kbd> followed by the letter representing the axis you want to rotate around (i.e. <kbd>X</kbd>, <kbd>Y</kbd> or <kbd>Z</kbd> and followed by <kbd>90</kbd> or <kbd>180</kbd> depending on the orientation. Then press <kbd>Enter</kbd> to finalize the transformation.
* Press <kbd>N</kbd> to bring up the properties panel. You can also click on the **+** sign on the upper right of the 3D View. In the panel that appears, click on the **Rotation** _![](../content/images/scia-blender-tutorial/rotations.jpg)_ spinner of the desired axis and enter the number in degrees.

You would also need to move your model so that it's Z zero is the same as Blender's Z zero. You can do that using the colored arrows at the origin of your model, or using the **Location** spinners as above.

Depending on your model, it might be good to add a ground plane. Choose **Add** > **Mesh** > **Plane** and on the left panel change **Radius** to something big. Move the plane as you wish using the colored arrows in the viewport.

## Lighting your model.

![](../content/images/scia-blender-tutorial/lights.jpg)

The dotted circle you see is a blender light. To see its effects on your model, you need to switch to **Material Viewport Shading** _![](../content/images/scia-blender-tutorial/viewport-shading.jpg)_ from the viewport shading menu.

Your model should now look something like the screenshot above. I have selected the pre-existing lamp and on the right you can see the **Light** panel selected in the **Properties** editor. The **Energy** spinner controls its intensity.

If you would like to see shadows, change the light type to **Sun**. You can duplicate the existing light to add more lights, or use the **Add** > **Lamp** _![](../content/images/scia-blender-tutorial/add-lamp.jpg)_ menu. Things are added where the 3D cursor (![ +&#8413; ](../content/images/scia-blender-tutorial/3Dcursor.jpg)) is so left click anywhere to position your cursor before adding.

> **TIP:** If shadows aren't visible even after changing the lamp type to **Sun** change momentarily from **Blender Render** to **Blender Game** _![](../content/images/scia-blender-tutorial/render-engine.jpg)_ and increase the lamp **Frustum Size**.

When you are happy with the lighting, switch to the cycles rendering engine, by choosing **Cycles Render** _![](../content/images/scia-blender-tutorial/render-engine.jpg)_ from the render engine menu. You will lose the shadow and lighting effects but you can always switch back to **Blender Render** to preview them.

##Making concrete look like concrete
Creating materials in blender or any other 3D graphics environment is a science of its own. In order to keep the size of this tutorial down, I will not delve into materials, but rather provide a ready made concrete material to use. Download [concrete.blend](../content/images/scia-blender-tutorial/concrete.blend) and append the concrete material it contains to your document by choosing **File** > **Append**. Choose the downloaded `.blend` file and then **Materials** > **Concrete** and click **Append from Library**.

Many thanks to [seier](https://www.flickr.com/photos/seier/4338268272) for the texture.

With the model selected you open the **material properties panel** _![](../content/images/scia-blender-tutorial/materials.jpg)_ and choose **Concrete** from the list. You can now choose the **Rendered** viewport shading to check out the result. Be patient. Rendering can take some time. If you have a capable graphics card you can turn on GPU rendering from the user preferences and the **Render** properties panel, and significantly improve performance.

##Photography
Time to set up our camera. Navigate your view to somewhere interesting and press <kbd>Space</kbd>. A search interface for blender commands will appear _![](../content/images/scia-blender-tutorial/space-menu.jpg)_. Type **Align** and choose **Align Camera To View** from the results.

Now you need to open the **Properties** panel if it is not already open by pressing <kbd>N</kbd> and check **Lock Camera to View** _![](../content/images/scia-blender-tutorial/lock-camera-to-view.jpg)_. The camera border turns red and you can now move the camera with you just as you navigate your model. Compose your photo and uncheck **Lock Camera to View** to exit the camera adjustment mode.

##Render
Press <kbd>F12</kbd> and wait for the tiles to fill up the screen. The default settings should be adequate but you can always bump up the quality at the expense of speed in the **Render** panel.

Save your image with <kbd>F3</kbd> or by clicking **Image** > **Save As Image**

![](../content/images/scia-blender-tutorial/render.jpg)

The second part of the tutorial will cover uploading to [sketchfab](http://sketchfab.com) and creating an interactive experience.

----

Written with [StackEdit](https://stackedit.io/).


<style>
em>img{
    display: none;
}
em::after{
    content: " ?\20DD";
}
em{
    font-style: normal;
    font-weight: bold;
}
em:hover>img{
    display: block;
    position:absolute;
    width: 800px;
    box-shadow: 3px 3px 3px rgba(0,0,0,0.5);
    margin-left: 5vw;
}
@media all and (max-width:800px) {
    em:hover>img{
        left: 0px;
    }
}
@media all and (max-width:500px) {
    em:hover>img{
        margin-left: 20px;
    }
}
</style>


