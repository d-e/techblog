```
Title: Create beautiful visualisations of your structural models with blender and sketchfab &mdash; part 2: interactive experience.
Description:
Author: Michael Demetriou
Date: 2017-06-16
```
--


If you'd like to present your 3D model to your clients, or even the public, the best way is to serve it through the web. The easiest way to publish a 3D model on the web is either through [sketchfab](http://sketchfab.com) or host it yourself using [blend4web](http://blend4web.com). Each has its own set of advantages/disadvantages.
###Sketchfab
* Built in blender
* One click publish
* Tweak lighting and materials after publication
* Embed in social networks
* Does not require hosting

###Blend4web
* Quicker to iterate (you don't have to wait for upload/preprocess)
* Works offline (if you are going to a meeting, that's important)
* Open Source
* You can host it yourself on your own server, requires having 


Unfortunately neither solution supports automatically mapped textures so we will have to do that ourselves.

This involves a process called UV unwrapping, which actually tells the renderer how to apply the texture on the 3d model. Blender has **Smart UV Project** which creates an acceptable UV Map.

If you need to apply specific parts of the image on specific faces of your model you will need to intervene (for example if you want the logo of your company on a specific wall) but **Smart UV Project** works okay with random textures such as concrete.

So let's get going: 

* Click on the layout chooser on blender's top bar. _![](../content/images/scia-blender-tutorial/part2/screen-layout.jpg)_
* Chose UV Editing
* On the right-hand window navigate to your object, select it and press <kbd>Tab</kbd> to enter Edit mode. (Alternatively choose **Edit Mode** from the mode selector at the bottom _![](../content/images/scia-blender-tutorial/part2/edit-mode.jpg)_)
* Once in edit mode make sure all nodes of the object are selected. If not, hit <kbd>A</kbd> to select/deselect all.
* Choose **Smart UV Project** and click **OK** on the dialog box that pops up. _![](../content/images/scia-blender-tutorial/part2/smart-uv-project.jpg)_
* On the left hand side you should now see the model unwrapped as a flat shape. Hit <kbd>Tab</kbd> again to exit edit mode. 

![](../content/images/scia-blender-tutorial/part2/unwrapped-concrete.jpg)

  > TIP: You can choose the concrete texture from the dropdown on the bottom left to see where each part will be projected. You can also choose **Paint** instead of **View**  _![](../content/images/scia-blender-tutorial/part2/paint.jpg)_ and paint directly on the image. Hit <kbd>T</kbd> to reveal the painting tools and don't forget to save the image afterwards **Image** > **Save All Images**.
  
Once you've finished, go back to the **Default** screen layout and we are ready to export.

##Sketchfab

In order to publish to sketchfab you can either use the built in addon, or just upload the `.blend` file. I prefer uploading the `.blend` file but if you insist, go to **File** > **User Preferences**, click **Addons**, search for sketchfab and enable it. If you want to keep it enabled next time you fire up blender click **Save User Settings**. _![](../content/images/scia-blender-tutorial/part2/preferences.jpg)_

On the **Tools** panel on the left a new tab called Sketchfab will appear. You will need to request an API key before you upload. _![](../content/images/scia-blender-tutorial/part2/sketchfab.jpg)_

Before uploading you need to click on **File** > **External Data** > **Pack all into .blend** _![](../content/images/scia-blender-tutorial/part2/pack-all.jpg)_ in order to make sure all the textures are included in your model.

When you are ready save your `.blend` file and either click **Upload** from the **Tools** panel or manually upload your file through [sketchfab.com](http://sketchfab.com)

##Blend4Web

The other solution requires downloading the [Blend4Web addon](https://www.blend4web.com/en/downloads/). You just need to download the addon, not the whole SDK: scroll down and click the **Download** link under Blender Add-on, it should not be more than a few Mb. Then you have to installing by going to **File** > **User Preferences** > **Addons** click on **Install from File** and choose the `.zip` file you just downloaded. Enable the addon and then click **Save User Settings**  _![](../content/images/scia-blender-tutorial/part2/preferences.jpg)_.

Due to a bug in sketchfab you have to make a change in the material properties. Switch to **Blend4Web** render settings  _![](../content/images/scia-blender-tutorial/render-engine.jpg)_, select your model, go to the **Material Properties** panels and uncheck **Use Nodes**  _![](../content/images/scia-blender-tutorial/part2/use-nodes.jpg)_

Next we have to enable shadows. With the building selected go to the **Object** properties panel (the orange cube _![](../content/images/scia-blender-tutorial/part2/shadows.jpg)_) and check the two boxes: **Cast Shadows** and **Receive Shadows**. Repeat the procedure for the ground plane if you have one.

Finally, choose **File** > **Export** > **Blend4Web (.html)** and save. Double click the resulting `.html` file to view in your favorite browser (must support WebGL, anything released after 2015 should support it). You can upload this file wherever you like. It is self-contained and portable.

![](../content/images/scia-blender-tutorial/part2/browsers.jpg)

Oh, and both solutions support VR. Give it a try if you have a headset.


<style>
em>img{
    display: none;
}
em::after{
    content: " ?\20DD";
}
em{
    font-style: normal;
    font-weight: bold;
}
em:hover>img{
    display: block;
    position:absolute;
    width: 800px;
    box-shadow: 3px 3px 3px rgba(0,0,0,0.5);
    margin-left: 5vw;
}
@media all and (max-width:800px) {
    em:hover>img{
        left: 0px;
    }
}
@media all and (max-width:500px) {
    em:hover>img{
        margin-left: 20px;
    }
}
</style>
