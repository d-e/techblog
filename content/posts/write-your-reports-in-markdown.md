```
Title: Write your reports in Markdown
Description:
Author: Michael Demetriou
Date: 2016-12-14
```
--

I hate word processors. I hate them because if I didn't know better I would bet
that they are non-deterministic. And after seeing how people use them, I hate
them even more.

I wish I could persuade everyone to use plain text but as that is impossible,
I just try to use plain text myself where possible. However being involved with
design stuff, I also love my styling.

Markdown with css is the way to go, but what if you need to adhere to the
company report template, or you need to share as "document" with some others
before finally outputting to pdf?

**Pandoc** to the rescue. I just discovered the awesome argument `--reference-odt`
which applies the stylesheet inside the reference `.odt` on your *markdown* report.
The upside is that features not available in your usual markdown editor, like
page numbers, headers/footers etc. are working nicely.

    pandoc report.md --reference-odt company_template.odt -o report.pdf

Now you can work in the non distracting world of plaintext, version your documents
in git and share drafts in email bodies, and convert to a nice looking, printable
document in the last minute. Finally you can output to pdf without ever seeing
a word-processor window using

    libreoffice --convert-to pdf --headless report.odt

Happy reporting!
