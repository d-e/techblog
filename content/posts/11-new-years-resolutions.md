```
Title: Tech team new year's resolutions
Description:
Author: Michael Demetriou
Date: 2019-01-07
```
--

## Happy New Year

![HAPPY 2019 over a glowing wireframe](../images/cyberpunk-scape.jpg)

It's 2019 but off-world colonies and nexus replicants are still ways away, but here we are planning once again for the future. This is a writeup of our tech team's general goals and plans for the year. The main theme is exploring new technologies, publishing our efforts and advancing engineering software.

## Consolidation

Being spread too thin is always a recipe for not accomplishing anything so I've decided to consolidate things a bit, though arguably these are still (over-?)ambitious goals. Our projects for the tech team here at *d-e* are three-fold:

### Rosalind

[Rosalind] is our home-grown [RepRap] 3D Printer and this year I'd like to see it grow beyond the boundaries of this office. The general goal is to make it the easiest printer to construct for someone that would like to build their first 3D printer.

*Rosalind* is parametric, meaning that you can build one with whatever size you like and choose the parts that will go in it (restrictions apply :-P ). It is also open source so everybody can improve on it.

We need to create instructions that are easy to follow, yet teach the builder the basics of 3D printing. Of course, a nice, friendly promotional website is required as well as a tool that will help builders configure their printer and list the required parts and materials. This tool can also potentially help us fund further development via affiliate links for procurement of equipment.

Further down the road we can consider building and selling kits or even pre-assembled 3D printers. 

Tangentially to this, there is an ongoing web development "course" which aims to teach web development while building the aforementioned friendly promotional website. You are invited to join the [web-developers-training repository] on [gitlab.com]

### Exploring BIM via Free-CAD

As we have mentioned in our final [Drafting through the Decades] blogpost we think [BIM] is the future of Computer Aided Design and since there is a [Free Software] BIM solution we definitely need to pursue it. We do not have a full, consistent digital representation of our new office building so this is a very nice way to start exploring the benefits of BIM. Since we have just moved in and we still have a lot of things to fix and improve this will help our day to day operations too.

We plan to document this exploration and share our experience with the world via live-streams, videos, tutorials and blog posts. Stay tuned.

### dx-stuff and Spanners

We started building [Free Software] modules for engineers and we are almost ready to publish our first contribution, `dx-punch`, a python module that tackles punching shear. We will have news on that soon. We've also decided to create a simple frontend for it and publish it on a platform called **Spanners** where engineers will be able to publish web applications that solve everyday problems. Along with `dx-punch` we will publish a small number of simpler utilities to populate **Spanners** with a few apps at launch.

We plan some of the **Spanners** to be paid apps so that we can fund development. Of course anyone can download and run locally as the code will be licensed under the AGPL.

### Addendum: git workflow

We have been trying to get to use git for all our files and projects. This has proven to be a challenge as along with the familiarization with the software we need to adapt our documentation and archiving habits (or lack thereof). In order to assist with this transition we have been working on a set of tutorials and workshops which we intend to share if everything goes according to plan.

## Onwards

Stay tuned for news and say hi on [twitter], [facebook], or [gitlab.com]

[Free Software]: https://www.gnu.org/philosophy/free-sw.en.html
[RepRap]: https://reprap.org/wiki/RepRap
[Rosalind]: https://reprap.org/wiki/Rosalind
[Drafting through the Decades]: https://medium.com/d-e-weblog/drafting-through-the-decades-series-ef7277da2eb
[web-developers-training repository]: https://gitlab.com/d-e/web-developers-training
[gitlab.com]:[https://gitlab.com]
[BIM]: https://en.wikipedia.org/wiki/Building_information_modeling
[twitter]: https://twitter.com/enginr_stories
[facebook]: https://facebook.com/demetriouengineering