```
Title: Pixelfed installation on ubuntu 16.04
Description:
Author: Michael Demetriou
Date: 2019-05-10
```
--

This is a guide on how to install pixelfed on ubuntu 16.04 since that is a distribution most probably to find on cheap VPS's.

## Preparation

In most servers this stuff is pre-installed, but when you just install ubuntu server with no extra options they are missing. There's no harm trying, worst that can happen is to get an "already installed" message.

In some cases (minimal default installation) you will need to add the `add-apt-repository` command. I also had to install the *https transport* for apt (that is a program that allows apt to download over *https* instead of *http*) because some newer repositories require it. Do this now because sometimes an installation might fail halfway through if it's not installed and that's a tough situation to get out of.

    sudo apt install python-software-properties software-properties-common apt-transport-https


Install git if you haven't got it already

    sudo apt install git

Also install bash completion, it will be helpful to autofill commands (use Tab)

    sudo apt install bash-completion

and the trash command so that you don't actually delete things permanently with `rm`

    sudo apt install trash-cli

Then you can use `trash useless-filename-here` to delete stuff.

I like the `micro` editor because it uses the modern GUI editor conventions in the terminal (such as Ctrl+C, Ctrl+V for copy and paste).

> The second command adds micro to PATH for all users because for some reason /snap/bin is not in the
path for every user.

    sudo snap install micro --classic
    sudo sed -i 's#"$#:/snap/bin"#' /etc/environment

or if that doesn't work

    curl https://getmic.ro | bash

## Installation of dependencies

First add php ppa's. This step adds newer php versions to this old ubuntu because *pixelfed* doesn't run with older ones.

    sudo add-apt-repository ppa:ondrej/php
    sudo add-apt-repository ppa:ondrej/apache2

(The ondrej apache ppa is not required but the ppa maintainer recommends adding it)

Then add mariadb (mysql-equivalent) repositories. The same goes here, *pixelfed* requires newer versions.

    sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
    sudo add-apt-repository 'deb [arch=amd64,arm64,i386,ppc64el] http://ftp.cc.uoc.gr/mirrors/mariadb/repo/10.2/ubuntu xenial main'

Refresh caches

    sudo apt update

Install php apache and mysql

    sudo apt install php7.2 apache2 mariadb-server

A prompt will appear that requires you to enter a MariaDB root password. Choose something secure and write it down somewhere safe.

Install other required modules

    sudo apt install imagemagick composer redis-server jpegoptim optipng pngquant php7.2-xml php7.2-pdo php7.2-json php7.2-ctype php7.2-xml php7.2-mbstring php7.2-gd php7.2-tokenizer php7.2-bcmath php7.2-curl php7.2-zip unzip php7.2-pdo php7.2-mysql

  <!--!!  [Add about apt cache search(?)]-->

Install mail transport agent so that pixelfed can send confirmation emails.

    sudo apt install postfix

In the dialog box that appears press Tab and then Enter to choose Ok and then choose Internet site from the list using the arrow keys on your keyboard and then press Enter to confirm. In the next page enter the name of your instance, e.g. myinstance.name.

At some point you will probably need to monitor processes and free memory so install htop before you actually need it, at which point will probably be impossible to do the installation

    sudo apt install htop

## Configuring apache

I use apache instead of nginx because that is the one I know better. I assume here that this is a single
purpose machine that only serves pixelfed. If you need to host more things I would advise to read up
on `php-fpm` or `suexec`. For our purposes plain-old `mod-php` will be enough.

Enable the rewrite and ssl modules

    sudo a2enmod rewrite
    sudo a2enmod ssl

I prefer to run apache as a user I can log in to so now we will do that:

Create a new user `pixelfed`

    sudo useradd -m pixelfed

Edit apache environment variables so that it runs using that user

    sudo micro /etc/apache2/envvars

Find and replace `export APACHE_RUN_USER=www-data` and `export APACHE_RUN_GROUP=www-data` with `export APACHE_RUN_USER=pixelfed` and `export APACHE_RUN_GROUP=pixelfed` respectively. Save (Ctrl+S) and close (Ctrl+Q) the file.

Change the web server root directory to the **public** directory of the pixelfed repository

    sudo micro /etc/apache2/sites-available/000-default.conf

Replace `/var/www/html` with `/home/pixelfed/pixelfed/public`.

Do not close the editor just yet, there's one more step. We need to tell apache it's permitted to serve files from this directory. Just below the `DocumentRoot` line, add the following directives

    <Directory /home/pixelfed/pixelfed/public>
        AllowOverride All
        Require all Granted
    </Directory>

Save and close the file.

## Configuring php

Change php max upload size (how large a file is allowed to be uploaded)

    sudo micro /etc/php/7.2/apache2/php.ini

Search the file for `upload_max_filesize` and `post_max_size` and change to 80M and 120M respectively. You can go larger and just configure pixelfed to limit the sizes but I think 80M is a reasonable size.
Save and close the file.

Restart apache

    sudo service apache2 restart

## Configuring MariaDB

<!--
Set a root password for your *mariadb* installation

   sudo mysql_secure_installation


The first question requires to enter the password for the root user of MariaDB, if you have set one, but we haven't, so press enter.
Then enter a secure password for your root user and make sure to keep it somewhere safe.
Follow the rest of the instructions and answer `yes` everywhere. 

This doesn't seem to be required on MariaDB 10.2
-->

Log in to mysql as root

    mysql -u root -p

Enter the mysql root password as you entered it above.

Create a pixelfed database and user and give the user permissions. On the prompt that appears

    CREATE SCHEMA pixelfed;
    CREATE USER 'pixelfed'@'localhost' IDENTIFIED BY 'supersecretpassword';
    USE pixelfed;
    GRANT ALL PRIVILEGES ON pixelfed.* TO 'pixelfed'@'localhost';
    FLUSH PRIVILEGES;
    EXIT;

## Installing Pixelfed

Log in as `pixelfed`

    sudo su pixelfed

change to the home directory

    cd

Get the code

    git clone https://github.com/pixelfed/pixelfed.git
    cd pixelfed

### Configuration

Copy the default configuration

    cp .env.example .env

Edit the configuration

    micro .env

Set your instance name and domain as you wish.

    APP_NAME="PixelFed Test"
    APP_URL=http://myinstance.name
    ADMIN_DOMAIN="myinstance.name"
    APP_DOMAIN="myinstance.name"
    SESSION_DOMAIN="myinstance.name"

Set the username and password of MariaDB as you have configured them above. Find the following lines and complete the values after the `=`

    DB_DATABASE=pixelfed
    DB_USERNAME=pixelfed
    DB_PASSWORD=supersecretpassword

Set the mail server configuration. Replace `print3d.social` with the name of your instance.

    MAIL_DRIVER=sendmail
    MAIL_HOST=localhost
    MAIL_PORT=2525
    MAIL_USERNAME=null
    MAIL_PASSWORD=null
    MAIL_ENCRYPTION=null
    MAIL_FROM_ADDRESS="pixelfed@myinstance.name"
    MAIL_FROM_NAME="myinstance.name"

Set the federation flags to true if you want

    ACTIVITY_PUB=false
    REMOTE_FOLLOW=false

Save and close the file.

## Deploying

Run the post deployment commands as outlined [here](https://docs.pixelfed.org/master/deployment.html)

    cd /home/pixelfed/pixelfed # Or wherever pixelfed is installed
    composer install --no-ansi --no-interaction --no-progress --no-scripts --optimize-autoloader
    php artisan key:generate
    php artisan config:cache
    php artisan route:cache
    php artisan migrate --force
    php artisan horizon:purge

Link the storage directory

    php artisan storage:link

<!-- Start horizon (This step is probably not required if you set up supervisor as below)

    php artisan horizon:start -->

Now we need to make sure horizon is always running. The best way would be with systemd user service but this doesn't want to cooperate, at least in ubuntu 16.04, so we'll use supervisor. Supervisor is a program that runs in the background and makes sure other programs run in the background, restarting them if needed. [Instructions taken from here](https://stackoverflow.com/questions/47911723/run-laravel-horizon-as-a-background-service). Drop out of the pixelfed account to an account with sudo rights.

    exit

## Installing services

You should now see a prompt with your original username (or root), such as `qwazix@myinstance.name`

Install supervisor to make sure horizon always runs

    sudo apt install supervisor

Make it run at startup

    sudo systemctl enable supervisor

Now create a new file in `/etc/supervisor/conf.d/` named `horizon.conf`

    sudo micro /etc/supervisor/conf.d/horizon.conf

and add the configuration below.

    [program:horizon]
    process_name=%(program_name)s
    command=php /home/pixelfed/pixelfed/artisan horizon
    autostart=true
    autorestart=true
    user=pixelfed
    redirect_stderr=true
    stdout_logfile=/home/pixelfed/pixelfed/horizon.log

Now do:

    sudo supervisorctl reread

to reread the configs

    sudo supervisorctl update

to reload the configs and restart the process

<!-- This also doesn't seem to be needed, it responds that
horizon is already started 

    sudo supervisorctl start all

or

    sudo supervisorctl start horizon

to start horizon 

-->

## Let's encrypt and https

Go to [certbot website](https://certbot.eff.org/lets-encrypt/ubuntuxenial-apache) and follow the instructions there. I copied them here for easy access:

    sudo add-apt-repository universe
    sudo add-apt-repository ppa:certbot/certbot
    sudo apt-get update
    sudo apt-get install certbot python-certbot-apache

    sudo certbot --apache

Certbot comes with a cronjob automating renewal so in theory you shouldn't need to do anything more at this time. Enjoy your pixelfed installation.

### In case you're doing this locally (do not do this if you've successfully used certbot above)

If you don't have a domain or your machine isn't on the internet you can use a self-signed certificate

    sudo a2ensite default-ssl
    sudo micro /etc/apache2/sites-enabled/default-ssl.conf

Change `DocumentRoot` and add the `<Directory>` section as we did [above](#configuring-apache). Don't forget to restart apache (see above for how to do that).

## Creating your first user and setting as adminstrator

Go to the website (http://your-instance.name) and register a new account
Go back to your vps and log on to mysql again

    mysql -u pixelfed -p

Give the supersecretpassword
In the prompt that appears make yourself adminstrator

    use pixelfed;
    update users set is_admin=1 where id=1;

If you have trouble seeing the activation email you can activate yourself too

    update users set email_verified_at="2019-02-12 10:25:32" where id=1;

## Final steps

Now go to the website again and log in. You should be able to administer the instance.

Happy posting!