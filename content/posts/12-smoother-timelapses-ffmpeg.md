```
Title: Smooth timelapses with ffmpeg
Description:
Author: Michael Demetriou
Date: 2019-02-10
```
--

## Smoothing timelapses with ffmpeg

During shooting of the [Rosalind videoseries] we had the idea to include a timelapse of our lunch in each episode. It's fun and it provides a kind of intermission in the middle of the video so our viewers can take a breath amid all the technical information.

These get to be quite long and it's nice to speed them up with this command:

    ffmpeg -i input.mkv -r 16 -filter:v "setpts=0.25*PTS" output.mkv

When you do however all motion blur goes away and the video looks kinda choppy. Increasing the framerate is one option but it doesn't work when you just want to include it in another video that's already 24fps.



You can make it smoother with *ffmpeg*'s *tblend* filter which averages the frames instead of dropping them when speeding up.

    ffmpeg -i input -vf "tblend=average,framestep=2,tblend=average,framestep=2,setpts=0.25*PTS" -r srcfps -{encoding parameters} output

Check out the following sequence.

![Frame sequence](../images/rosalind-food-frames.jpg)

Instead of choosing one image from that sequence like the first command does,

![Frame sequence](../images/rosalind-food-f000027.jpg)

The second command averages the 4 frames and creates an image that resembles motion blur


![Frame sequence](../images/rosalind-food-smooth.jpg)

However 4 sharp frames are not enough to produce a really motion-blurry result. You can clearly see ghost images of the four frames. So what can we do?

We don't have enough frames for a smoother result but ffmpeg can approximate them for us. The *minterpolate* filter to the rescue:

    ffmpeg -i input -filter:v minterpolate -r 96 output

This will create a 96fps video with interpolated intermediate frames. Now we can average 16 frames instead of four

    ffmpeg -i input -vf "tblend=average,framestep=2,tblend=average,framestep=2,tblend=average,framestep=2,tblend=average,framestep=2,setpts=0.25*PTS" -r 96 -b:v 30M -crf 10 -an output

and get a very nice and smooth result

![Frame sequence](../images/rosalind-food-smoother.jpg)

<div style=width:100vw>
<video controls autoplay loop>
    <source src="../videos/rosalind-food-choppy.webm" type="video/webm">
    use a browser that suports webm to see a video here
</video><video controls autoplay loop>
    <source src="../videos/rosalind-food-smooth.webm" type="video/webm">
    use a browser that suports webm to see a video here
</video><video controls autoplay loop>
    <source src="../videos/rosalind-food-smoother.webm" type="video/webm">
    use a browser that suports webm to see a video here
</video>
</div>

[Rosalind videoseries]: https://www.youtube.com/channel/UCdxG_ejbPIiGv8sVioCvBeA

<style>
video{
    width: 600px !important;
    display: inline-block !important;
}
</style>