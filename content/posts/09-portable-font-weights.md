```
Title: Portable multiple font weights in LibreOffice and AutoCAD.
Description:
Author: Michael Demetriou
Date: 2017-07-10
```
--


## What's wrong with the situation as it is?

LibreOffice for mac recognizes (as it should) font weights other than bold as styles of the same font and not as separate fonts. I.e. fonts can have than the 4 standard styles. (Regular, Italic, Bold, Bold Italic).

On windows the system does not support anything other than the 4 standard styles so other weights appear as different fonts (E.g. *Source Sans Pro Light*).

Thus this causes incompatibility between platforms. A style set to 
*Source Sans Pro Light* in Windows does not show up correctly on a Mac.

> While GNU/Linux does support multiple styles, LibreOffice on G/L behaves the same as on windows.

Font files work around this issue by specifying the family name as `Source Sans Pro Light` for platforms that do not support multiple weights and a *Preferred Family* name (`Source Sans Pro`), ignored by Windows, which allows other platforms to group by the greater family, accompanied with a *Preferred Style* that specifies the extra styles (`Light`).

Inkscape solves this problem by storing both `Source Sans Pro` with weight of `Light` and `Source Sans Pro Light` and chooses whatever is available. 

In LibreOffice however when you save a document on Windows it saves  `Source Sans Pro Light` (the font name as reported by Windows) and when you try to open it on the mac it searches for that value in the liset of fonts reported by macOS (i.e. in the list of *Preferred Families*) and fails to find it.

Thus we modified the family to report to MacOS the full family name 
(i.e. Preffered Family Name is now the same as Family Name). Installing both 
the unmodified fonts (for inkscape and other programs which work as they should)
and the modified fonts at the same time clutters your font list a bit but works
around the problem until LibreOffice fixes the bugs referenced in this [question][1] or Microsoft decides that they should finally support more than 4 font styles.

[1]:(https://ask.libreoffice.org/en/question/27088/font-bug-on-lo-41-when-opening-documents-created-with-lo-40/?answer=27111#post-id-27111) 

AutoCAD for Mac also does not list all the font weights and supports only the classic 4.
Thus, in order to use light fonts we have to get them listed in the font selector
as separate fonts. The following procedure works around both issues.

## How to update special font types for Mac using fontforge

* Open [fontforge](https://fontforge.github.io/)
* Open the font which you want to create a Mac version for. We'll use the excellent [Source Sans Pro](https://github.com/adobe-fonts/source-sans-pro) for this example.
* From the menu select **Element** --> **Font Info**

> If you are not using LibreOffice but just AutoCAD for Mac, changing the PS Fontname is not required. The font will still show as a 
> different font in AutoCAD but will not clutter your font list anywhere else. (I assume AutoCAD does it's own font indexing instead of relying on the OS)

* go to tab **PS Names**
   * in **Fontname** : give a new name to the font, for example `Source Sans Pro Mac` (This is required in order for *macOS* to recognize the new font as a different family. Otherwise it sees it as conflicting with the original *Source Sans Pro* and just activates one or the other.)
  * For Regular types: **family name** and **name for humans** should be identical. `Source Sans Pro Light`
  * For Italic types: **family name** is different than **name for humans**. The world _Italic_ should only be present in the **name for humans** field. `Source Sans Pro Light Italic`
  ![](../content/images/fontforge-mac.png)
* go to tab **TTF names**. We will update **Preferred Family** and **Preferred Styles**.
  * Change **Preferred Family** to be identical with **Family** (`Source Sans Pro Light` instead of `Source Sans Pro`)
  * **Preferred Styles** should be identical with **Styles** (`Regular` instead of `Light`)
* Select *OK*, You will receive a message regarding the UniqueID. Select *Change*.
* From the menu select **File** --> **Generate Fonts...**
  * make sure tha the type is selected in *OpenType (TTF)*
  * Select *Generate* (you may receive a message regarding the length of the name. update as required and return to this step).
  * You will receive a message regarding detected errors (missing points in Extrema).
  * Select *Generate*.
  * Finally select *OK*.

Now you can just install the new ttf/otf files on your mac and have no more issues viewing documents with light fonts from other platforms. Take note that as of AutoCAD for Mac 2015 it didn't support *otf*, only *ttf*. 

