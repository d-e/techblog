```
Title: Cast from any computer to a raspberry pi using vnc
Description:
Author: Michael Demetriou
Date: 2017-05-04
```
--

We have all kinds of computers in the office. Some of them support airplay, some Miracast and some none of the above. I wanted a universal solution that works for all these computers, wired or wireless regardless of OS or hardware. Long-standing protocols in the rescue, once again:

A raspberry pi connected to the office tv along with vncviewer and any vnc server on the client will do.

[Here](https://gitlab.com/d-e/vnc-cast) are some scripts that make this whole thing a one-click process. Remember there's no sanitization or encryption going on. It's for use behind the company firewall.

<video src="../images/vnc.mp4" autoplay loop>
Sorry, your browser doesn't support embedded videos,
but don't worry, you can <a href="../images/vnc.mp4">download it</a>
and watch it with your favorite video player!
</video>
