```
Title: Export high quality images from draftsight or autocad to (libre) office
Description:
Author: Michael Demetriou
Date: 2017-01-13
Status: Published
```
--

We have been having issues with exporting things from drawings to reports. Either the quality is not good enough, or it involves too many steps.

One solution that works well and that doesn't involve too much effort is to go through pdf. Pdf export is usually very good in most CAD programs and from there, you can easily convert to image in any resolution you might want.

Why not export directly to png, you ask. Because usually resolution/crop is dependent on the monitor resolution and window size, which produces awful results except if you have a super high resolution display.

### Hence, on to the how-to:

First export the part of the drawing you want to `pdf`. *DraftSight* has a built in function for that and you can always just print to pdf using *Microsoft pdf printer* or *bullzip*.

<a href="../content/images/how-to-import-dwg-to-odt/01 Draftsight - Export as pdf.png"><img src="../images/how-to-import-dwg-to-odt/01 Draftsight - Export as pdf.png" alt="From DraftSight to pdf" class="halfwidth"></a> <a href="../content/images/how-to-import-dwg-to-odt/02 Draftsight - PDF Export dialog.png" ><img src="../images/how-to-import-dwg-to-odt/02 Draftsight - PDF Export dialog.png" class="halfwidth"></a>

Now you have to open *the GIMP* (which is our choice here because it can read pdf's pretty well) and open the pdf.

<a href="../content/images/how-to-import-dwg-to-odt/03 Gimp import from pdf.png" ><img src="../images/how-to-import-dwg-to-odt/03 Gimp import from pdf.png" class="right" style="width: 180px" ></a>

In the dialog that pops up, you need to choose a resolution that is good enough for print at the intended size. An easy way to do that is to keep the *resolution* field at 300 pixels/in (dpi) and play with *width* or *height* until the size in inches approximates the size you will finally print on; e.g. if your drawing will cover a quarter of a page in your final report, you'll need about 4x5 inches or 100x150mm.

Another rule of thumb is this:

 > Multiply the size of the figure in *mm* by 10, and you got the pixels you need.
 
 **i.e.** for a figure printed around 100x100 mm you need an image of 1000x1000px 

<a href="../content/images/how-to-import-dwg-to-odt/04 Gimp Crop Tool.png" ><img src="../images/how-to-import-dwg-to-odt/04 Gimp Crop Tool.png" class="halfwidth"></a>

After hitting *Import* you get the pdf as image inside the GIMP. Now it's time to save it as `.png` and import it in *LibreOffice* or *Ms Office*.

<a href="../content/images/how-to-import-dwg-to-odt/05 Gimp Export png.png" ><img src="../content/images/how-to-import-dwg-to-odt/05 Gimp Export png.png" class="halfwidth" style="clear: both"></a>
<a href="../content/images/how-to-import-dwg-to-odt/06 Writer.png" ><img src="../images/how-to-import-dwg-to-odt/06 Writer.png" class="halfwidth"></a>
