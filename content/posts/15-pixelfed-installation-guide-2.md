```
Title: Pixelfed installation on ubuntu 18.04
Description:
Author: Michael Demetriou
Date: 2019-11-07
```
--

This is a guide on how to install pixelfed on ubuntu 18.04 since that is a distribution most probably to find on cheap VPS's.

## Preparation

In most servers this stuff is pre-installed, but when you just install ubuntu server with no extra options they are missing. There's no harm trying, worst that can happen is to get an "already installed" message.

In some cases (minimal default installation) you will need to add the `add-apt-repository` command. I also had to install the *https transport* for apt (that is a program that allows apt to download over *https* instead of *http*) because some newer repositories require it. Do this now because sometimes an installation might fail halfway through if it's not installed and that's a tough situation to get out of.

    sudo apt install software-properties-common apt-transport-https

and the trash command so that you don't actually delete things permanently with `rm`

    sudo apt install trash-cli

Then you can use `trash useless-filename-here` to delete stuff.

I like the `micro` editor because it uses the modern GUI editor conventions in the terminal (such as Ctrl+C, Ctrl+V for copy and paste).

> The second command adds micro to PATH for all users because for some reason /snap/bin is not in the
path for every user.

    sudo snap install micro --classic
    sudo sed -i 's#"$#:/snap/bin"#' /etc/environment

or if that doesn't work

    curl https://getmic.ro | bash

## Installation of dependencies

First add php ppa's. This step adds newer php versions to this old ubuntu because *pixelfed* doesn't run with older ones.

    sudo add-apt-repository ppa:ondrej/php

(The ondrej apache ppa is not required but the ppa maintainer recommends adding it)

Then add mariadb (mysql-equivalent) repositories. The same goes here, *pixelfed* requires newer versions.

    sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
    sudo add-apt-repository 'deb [arch=amd64,arm64,i386,ppc64el] http://ftp.cc.uoc.gr/mirrors/mariadb/repo/10.2/ubuntu xenial main'

Refresh caches

    sudo apt update
    
Upgrade what can be upgraded

    sudo apt upgrade

Answer yes to the prompt

Install nginx

    sudo apt install nginx

Install php and mysql

    sudo apt install php7.3 mariadb-server

You might need to remove apache.

    sudo apt remove apache2

If it says it's not installed don't worry about it, all is well.

Install other required modules

    sudo apt install imagemagick composer redis-server jpegoptim optipng pngquant php7.3-xml php7.3-pdo php7.3-json php7.3-ctype php7.3-xml php7.3-mbstring php7.3-gd php7.3-tokenizer php7.3-bcmath php7.3-curl php7.3-zip unzip php7.3-pdo php7.3-mysql php7.3-intl php7.3-fpm

  <!--!!  [Add about apt cache search(?)]-->

Install mail transport agent so that pixelfed can send confirmation emails.

    sudo apt install postfix

In the dialog box that appears press Tab and then Enter to choose Ok and then choose Internet site from the list using the arrow keys on your keyboard and then press Enter to confirm. In the next page enter the name of your instance, e.g. myinstance.name.

At some point you will probably need to monitor processes and free memory so install htop before you actually need it, at which point will probably be impossible to do the installation

    sudo apt install htop

## Configuring nginx

I prefer to run nginx as a user I can log in to so now we will do that:

Create a new user `pixelfed`

    sudo useradd -m pixelfed

Set his default shell to bash

    sudo chsh pixelfed -s /bin/bash

Edit apache nginx config so that it runs using that user

    sudo micro /etc/nginx/nginx.conf

Find and replace `user nginx;` with `user pixelfed;`. Save (Ctrl+S) and close (Ctrl+Q) the file.

Also we need to change the `php-fpm` user to `pixelfed`

    sudo micro /etc/php/7.3/fpm/pool.d/www.conf

Find all occurences of `www-data` and change them to `pixelfed` (use <kbd>Ctrl</kbd>+<kbd>F</kbd>). There are two on lines 23 and 24 also two on 47 and 48

Change the web server root directory to the **public** directory of the pixelfed repository

    sudo micro /etc/nginx/sites-available/default

Replace `/var/www/html` with `/home/pixelfed/pixelfed/public`.

Being here, you should also uncomment the php directives

        #location ~ \.php$ {
        #       include snippets/fastcgi-php.conf;
        #
        #       # With php-fpm (or other unix sockets):
        #       fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
        #       # With php-cgi (or other tcp sockets):
        #       fastcgi_pass 127.0.0.1:9000;
        #}

should become

        location ~ \.php$ {
               include snippets/fastcgi-php.conf;
        
               # With php-fpm (or other unix sockets):
               fastcgi_pass unix:/var/run/php/php7.-fpm.sock;
        #       # With php-cgi (or other tcp sockets):
        #       fastcgi_pass 127.0.0.1:9000;
        }

Notice that two of the # remain and that `php7.0` became `php7.3` 

## Configuring php

Change php max upload size (how large a file is allowed to be uploaded)

    sudo micro /etc/php/7.3/apache2/php.ini

Search the file for `upload_max_filesize` and `post_max_size` and change to 80M and 120M respectively. You can go larger and just configure pixelfed to limit the sizes but I think 80M is a reasonable size. You can also increase the memory limit to 512M or maybe half your vps's  RAM.
Save and close the file.

Restart nginx and php-fpm

    sudo systemctl restart nginx
    sudo systemctl restart php7.3-fpm.service 
    <!-- sudo service nginx restart -->

## Configuring MariaDB

<!--
Set a root password for your *mariadb* installation

   sudo mysql_secure_installation


The first question requires to enter the password for the root user of MariaDB, if you have set one, but we haven't, so press enter.
Then enter a secure password for your root user and make sure to keep it somewhere safe.
Follow the rest of the instructions and answer `yes` everywhere. 

This doesn't seem to be required on MariaDB 10.2
-->

Log in to mysql as root

    sudo mysql

Enter the mysql root password as you entered it above.

Create a pixelfed database and user and give the user permissions. On the prompt that appears

    CREATE SCHEMA pixelfed;
    CREATE USER 'pixelfed'@'localhost' IDENTIFIED BY 'supersecretpassword';
    USE pixelfed;
    GRANT ALL PRIVILEGES ON pixelfed.* TO 'pixelfed'@'localhost';
    FLUSH PRIVILEGES;
    EXIT;

## Installing Pixelfed

Log in as `pixelfed`

    sudo su pixelfed

change to the home directory

    cd

Get the code

    git clone https://github.com/pixelfed/pixelfed.git
    cd pixelfed

### Configuration

Copy the default configuration

    cp .env.example .env

Edit the configuration

    micro .env

Set your instance name and domain as you wish.

    APP_NAME="PixelFed Test"
    APP_URL=http://myinstance.name
    ADMIN_DOMAIN="myinstance.name"
    APP_DOMAIN="myinstance.name"
    SESSION_DOMAIN="myinstance.name"

Set the username and password of MariaDB as you have configured them above. Find the following lines and complete the values after the `=`

    DB_DATABASE=pixelfed
    DB_USERNAME=pixelfed
    DB_PASSWORD=supersecretpassword

Set the mail server configuration. Replace `myinstance.name` with the name of your instance.

    MAIL_DRIVER=sendmail
    MAIL_HOST=localhost
    MAIL_PORT=2525
    MAIL_USERNAME=null
    MAIL_PASSWORD=null
    MAIL_ENCRYPTION=null
    MAIL_FROM_ADDRESS="pixelfed@myinstance.name"
    MAIL_FROM_NAME="myinstance.name"

Set the federation flags to true if you want

    ACTIVITY_PUB=false
    REMOTE_FOLLOW=false

Save and close the file.

## Deploying

Run the post deployment commands as outlined [here](https://docs.pixelfed.org/installing-pixelfed/installation.html#final-steps)

    cd /home/pixelfed/pixelfed # Or wherever pixelfed is installed
    composer install --no-ansi --no-interaction --no-progress --no-scripts --optimize-autoloader
    php artisan key:generate
    php artisan config:cache
    php artisan route:cache
    php artisan migrate --force
    php artisan horizon:purge

Link the storage directory

    php artisan storage:link

<!-- Start horizon (This step is probably not required if you set up supervisor as below)

    php artisan horizon:start -->

Now we need to make sure horizon is always running. 

## Systemd setup

Exit the user shell

    exit

now create a new *systemd* service 

    sudo micro /etc/systemd/system/pixelfed.service

with the following contents

    [Unit]
    Description=Pixelfed task queueing via Laravel Horizon
    After=network.target
    Requires=mariadb
    Requires=php-fpm
    Requires=redis
    Requires=nginx

    [Service]
    Type=simple
    ExecStart=/usr/bin/php /home/pixelfed/artisan horizon
    User=pixelfed
    Restart=on-failure

    [Install]
    WantedBy=multi-user.target

enable the service

    sudo systemctl enable pixelfed

and start it

    sudo systemctl start pixelfed

## Launch!

restart nginx

    sudo systemctl restart nginx

## Let's encrypt and https

Go to [certbot website](https://certbot.eff.org/lets-encrypt/ubuntuxenial-apache) and follow the instructions there. I copied them here for easy access:

    sudo apt-get install certbot python-certbot-nginx

    sudo certbot --nginx

Follow the instructions from there.

Certbot comes with a cronjob automating renewal so in theory you shouldn't need to do anything more at this time. Enjoy your pixelfed installation.

## Creating your first user and setting as adminstrator

Go to the website (http://myinstance.name) and register a new account
Go back to your vps and log on to mysql again

    mysql -u pixelfed -p

Give the supersecretpassword
In the prompt that appears make yourself adminstrator

    use pixelfed;
    update users set is_admin=1 where id=1;

If you have trouble seeing the activation email you can activate yourself too

    update users set email_verified_at="2019-02-12 10:25:32" where id=1;

## Final steps

Now go to the website again and log in. You should be able to administer the instance.

Happy posting!