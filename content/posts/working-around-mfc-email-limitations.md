```
Title: Working around the Xerox 7120 limitation of missing SHA-2 encryption
Description: This also applies to the Lexmark x543dn and possibly other outdated MFC's
Author: Michael Demetriou
Date: 2016-12-14
```
--

We have a *Xerox Workcentre 7120* here in the office. We used to have *scan2email* configured with the [Gmail](http://gmail.com) server, but this started failing somewhere around 2014 when Google deprecated `SHA-1` encryption.

In the meantime we were using another server which was recently decommissioned, and nowadays it's a bit difficult to locate a mail server that supports unencrypted connections or old encryption algorithms, and to be frank, the big services have deprecated `SHA-1` for a reason, so I didn't really want to.

There is however a relatively secure solution that does not require purchasing a new copier. Go get a [raspberry pi](http://raspberrypi.org) (even the lowly original Model B with 256mb ram will do) and install *raspbian* on it.

### Raspberry pi configuration

After booting up `sudo apt install postfix` and then edit `/etc/postfix/main.cf`.

Find the line that starts with `mynetworks` and add the LAN IP of your MFC at the end, like this

from:

    mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128

to:

    mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 192.168.3.253

or, to allow the whole lan

    mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 192.168.3.0/8

### Xerox configuration

Now set the SMTP server of the Xerox to the ip of your Raspberry Pi and set no encryption and no authentication. As you are behind the NAT sending mail is still secure, unless you fear that someone on the inside of your NAT will start spamming away, and even in that case, you can restrict the ip to the one of your copier.

I haven't tried this, but adding a plaintext password should be easy if you still need more security. Adding TLS should be harder as it probably requires a domain name, so you'll have to either live with the red padlock in Gmail or expose your raspberry to the internet which increases the security surface tremendously [EDIT: , or better, do the smarthost thing that @tbr23 suggested. - see below]

This means that you are not certain someone didn't tamper with your email in transport, but you are pretty sure that your password isn't compromised. As our machine nears EOL I'm willing to live with that for a few months, until we get a new one.

### Ps

There is a slight chance that I changed something in master.cf too but I am not sure. Ping me [@qwazix](http://twitter.com/qwazix) on twitter or fire an email to mike[a]d-e.gr as this blog doesn't have comments.

### Ps II

@tbr23 has this very good idea: I might try it if I find some time before we upgrade the whole copier

<blockquote class="twitter-tweet" data-conversation="none" data-lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/qwazix">@qwazix</a> Might want to set gmail as a smarthost. That will fix your red padlock etc.</p>&mdash; Thomas B. Rücker (@tbr23) <a href="https://twitter.com/tbr23/status/809059593020129281">December 14, 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

### Ps III

I forgot to mention that I added an **spf** record to my domain allowing the office IP to send emails under the company domain, in order to avoid possible spam filtering. It also includes *google* as the normal email operations are handled via *Google Apps*. (Now *G-Suite*. Really Google? G-Suite?)

    d-e.gr.     IN TXT   "v=spf1 mx a:178.59.99.205 include:_spf.google.com ~all"
