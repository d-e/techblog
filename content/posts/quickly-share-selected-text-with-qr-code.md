```
Title: Quickly share selected text with qr code on Linux
Description:
Author: Michael Demetriou
Date: 2016-12-14
```
--

Install `xclip` and `python-qrcode`, go to your DE's shortcuts applet and add this to any hotkey you fancy

    xclip -o | qr | display

I chose <kbd>Super</kbd> + <kbd>C</kbd> and when I hit it, any text I have selected pops up like this:

<img src=../images/qrcode.jpg alt="qr code in action">
